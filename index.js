console.log("Hi");

console.log(document);
// result: document HTML code


// Activity Solution

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
let spanFullName = document.querySelector("#span-full-name");

const event = (e) => {
	spanFullName.innerHTML = txtFirstName.value + ` ` + txtLastName.value
};

txtFirstName.addEventListener('keyup', event);
txtLastName.addEventListener('keyup', event);